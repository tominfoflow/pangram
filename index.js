'use strict';

const pino = require('hapi-pino');
const blipp = require('blipp');

const Hapi = require('@hapi/hapi');
const routes = require('./lib/routes/routes');
const connectMongo = require('./lib/common/database');
const inert = require('@hapi/inert');
const vision = require('@hapi/vision');
const hapiSwagger = require('hapi-swagger');

// const anagram_dictionary = require('./lib/anagram/anagram')
// const readStreamWord = require('./lib/anagram/anagram_groups')
// const isogramEnglishWord = require('./isogram_english_word')

const init = async () => {
    const server = Hapi.server({
        port: process.env.PORT || 3000,
        host: '0.0.0.0',
    });    

    const swaggerOptions = {
        info : {
            title : 'Pangram Api Documentation',
        }
    }

    await server.register([
        inert,
        vision,
        blipp,
        {
            plugin : hapiSwagger,
            options : swaggerOptions
        }, 
        {
            plugin : pino,
            options : {
                prettyPrint : process.env.NODE_ENV !== 'production',
                logEvents : ['response, onPostStart']
            }
        }
    ]);

    await server.start();
    server.logger().info('Server running on %s', server.info.uri);

    server.route(routes);
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
})

init();
connectMongo();