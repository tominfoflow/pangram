const fs = require('fs');
const readStream = fs.createReadStream('words.txt', 'utf8');

const isogram = require('./lib/isogram/isogram');
const isogramModel = require('./lib/models/isogram');
const isogramSchema = require('./lib/models/schemas/isogram');

const objs = [];
readStream.on('data', async function (data) {
    const datas = data.split('\n');
    datas.every(async element => {
        const obj = new isogramSchema({ _id: element, word: element, isogram: isogram.isIsogram(element)});
        objs.push(obj);
    });
    // datas.forEach(async element => {
    //     const obj = await new isogramSchema({ _id: element, word: element, isogram: isogram.isIsogram(element)});
    //     objs.push(obj);
    // });
}).on('end', async function () {
    isogramModel.addIsograms(objs);
    console.log('finish isogram english words');
});

module.exports = readStream;