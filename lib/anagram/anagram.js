const anagram = {}
const checkEnglishWord = require('./check_english_word')

anagram.isAnagram = async function (word1, word2) {
    if (await anagram.checkLength(word1, word2)) {
        const arrayWord1 = anagram.sortWord(word1.toLowerCase());
        const arrayWord2 = anagram.sortWord(word2.toLowerCase());

        if (await checkEnglishWord(word1) == false) {
            return word1 + ' is not english word.';
        }

        if (await checkEnglishWord(word2) == false) {
            return word2 + ' is not english word.';
        }

        if (await JSON.stringify(arrayWord1) === JSON.stringify(arrayWord2)) {
            return word1 + ' and ' + word2 + ' are anagram.'
        };
    }
    return word1 + ' and ' + word2 + ' are NOT anagram.';
}

anagram.checkLength = (word1, word2) => {
    return word1.length == word2.length;
}

anagram.sortWord = function (text) {
    const list = [...text].sort(anagram.asc)
    var res = '';
    list.forEach(element => {
        res = res + element;
    });
    return res;
    
    // return [...text].sort(anagram.asc);
}

anagram.asc = (a, b) => {
    if (a < b) {
        return -1;
    } else if (a > b) {
        return 1
    }
    return 0;
}

module.exports = anagram;