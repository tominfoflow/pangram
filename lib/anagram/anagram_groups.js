const AnagramSchema = require('../models/schemas/anagram');
const AnagramModel = require('../models/anagram')
//const routes = require('../routes/routes');

const fs = require('fs');

const readStream = fs.createReadStream('words.txt', 'utf8');
const anagram = require('./anagram');

const group = {};
const groups = [];

readStream.on('data', function (data) {
    const datas = data.split('\n');
    datas.forEach(element => {
        const elementLowercase = element.toLowerCase();
        const key = anagram.sortWord(elementLowercase);
        const words = group[key] ? group[key] : [];
        words.push(elementLowercase);
        group[key] = words;

        // const obj = new AnagramSchema({key: key, anagrams: elementLowercase })
        // AnagramModel.createAnagramPairs(obj);
    });
}).on('end', async function () {
    const keys = Object.keys(group);
    keys.forEach(element => {
        const obj = new AnagramSchema({ _id: element, key: element, anagrams: group[element] })
        groups.push(obj);
    });

    // const obj = new AnagramSchema({key : 'abc', anagrams : ['abc, bca']});
    await AnagramModel.createListAnagramPairs(groups);
    console.log('finish');
})

module.exports = readStream