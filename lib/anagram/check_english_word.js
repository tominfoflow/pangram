const axios = require('axios').default;

const checkEnglishWord = async(word) => {
    try {
        const response = await axios.get('http://www.anagramica.com/lookup/'+word);        
        return response.data.found === 1 ? true : false;
    } catch (error) {
        console.log(error);
    }
}

module.exports = checkEnglishWord;