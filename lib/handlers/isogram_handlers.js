const isogram = require('../isogram/isogram')
const isogramHandlers = {};
const isogramModel = require('../models/isogram')

isogramHandlers.isIsogram = function(request, h){
    return isogram.isIsogram(request.params.word);
}

isogramHandlers.randomIsogram = function(request, h){
    return isogramModel.randomIsogram();
}

module.exports = isogramHandlers;