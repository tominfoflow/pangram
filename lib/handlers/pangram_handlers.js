const pangram_mongo = require('../models/pangram_mongo')
const pangramModel = require('../models/pangram')

const internals = {};

internals.getPangramHandlers = async function (request, h) {
    // const result = await pangram_mongo.getRandomPangramMongo();
    // return result;

    const result = await pangramModel.fetchPangram(request, h);
    return result;
}

internals.homeHandler = async function (request, h) {
    return 'Welcome to Pangram App ' + (request.params.user ? request.params.user : '');
}

module.exports = internals