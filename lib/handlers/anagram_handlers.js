const anagram = require('../anagram/anagram')
const anagramModel = require('../models/anagram');
const anagramHandlers = {}

anagramHandlers.isAnagramHandler = async function (request, h) {    
    return await anagram.isAnagram(request.params.word1, request.params.word2);
}

anagramHandlers.addListAnagram = async function(request, h){
    const payload = request.payload;

    return await anagramModel.createAnagramPairs(payload);
}

anagramHandlers.getListAnagram = async function(request, h){
    return await anagramModel.getListAnagram();
}

module.exports = anagramHandlers;