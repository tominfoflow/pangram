const mongoose = require('mongoose');

const url = 'mongodb+srv://admin:admin@cluster0-zbtkd.mongodb.net/Cluster0?retryWrites=true&w=majority';

const connectMongo = function (){
    mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}).then(() => {        
        console.log('Database connection successfull');
    }).catch((err) => {
        console.log('Database connection error');
    });
}

module.exports = connectMongo