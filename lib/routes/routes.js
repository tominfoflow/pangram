const handlers = require('../handlers/pangram_handlers');
const anagramHandlers = require('../handlers/anagram_handlers');
const isogramHandlers = require('../handlers/isogram_handlers');

getPangramRoute = {
    method: 'GET',
    path: '/pangram',
    options: {
        description: 'Get Pangram Random',
        notes: 'Returns Pangram of the day',
        handler: handlers.getPangramHandlers,
        tags: ['api'],
        validate: {

        }
    },
    // handler : (request, h) => handlers.getPangramHandlers(request, h),
}

homeRoute = {
    method: 'GET',
    path: '/{user?}',
    options: {
        tags: ['api']
    },
    handler: (request, h) => handlers.homeHandler(request, h),
}

isAnagramRoute = {
    method: 'GET',
    path: '/anagram/{word1}/{word2}',
    options: {
        tags: ['api']
    },
    handler: anagramHandlers.isAnagramHandler
}

addListAnagram = {
    method: 'POST',
    path: '/anagram/add',
    options: {
        tags: ['api'],
    },
    handler: anagramHandlers.addListAnagram
}

getListAnagram = {
    method: 'GET',
    path: '/anagram/list',
    options: {
        tags: ['api'],
    },
    handler: anagramHandlers.getListAnagram
}

isIsogramRoute = {
    method: 'GET',
    path: '/isogram/{word}',
    options: {
        tags: ['api'],
    },
    handler: isogramHandlers.isIsogram
}

randomIsogram = {
    method: 'GET',
    path: '/isogram/random',
    options: {
        tags: ['api'],
    },
    handler: isogramHandlers.randomIsogram
}

module.exports = [getPangramRoute, homeRoute, 
    isAnagramRoute, addListAnagram, getListAnagram, 
    isIsogramRoute, randomIsogram]