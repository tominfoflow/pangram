const fs = require('fs');
const insertMongo = require('./mongodb');
const uniqid = require('uniqid');

const readStream = fs.createReadStream('The Adventures of Sherlock Holmes.txt', 'utf8');
// const sentences = [];
const sentencePangram = [];

readStream.on('data', function(data){
    process(data);
}).on('end', function () {
    console.log(sentencePangram.length);
})

const process = function (data) {
    const datas = data.split('.');
//    sentences.push(datas);
    var text = '';
    datas.forEach(element => {
        text += element;
        if(isPangram(text)){
            sentencePangram.push(text);
            insertMongo(generateData(uniqid(), text));
            text = '';
        }
    });
}

const generateFile = function (text) {
    const file = new File([text], "filePangram.txt", { type: "text/plain", lastModified: Date.now });
}

/*
const isPangram = function (text){
    const uniqString = new Set();
    [...text].every(x => {
        if(matchAlphabet(x)){
            uniqString.add(x);
        }
    });
    return uniqString.size === 26;
}
*/

const isPangram = function (text) {
    return [..."abcdefghijklmnopqrstuvwxyz"].every(x => text.includes(x));
}

const matchAlphabet = function (char) {
    const letter = /^[A-Za-z]+$/;
    if (char.match(letter))
        return true;
    return false;
}

const generateData = function(id, text){
    return {
        "_id" : id, 
        "text" : text
    }
}

module.exports = { isPangram, matchAlphabet }