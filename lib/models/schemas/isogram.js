const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const isogramSchema = new Schema({
    word : String,
    isogram : Boolean,
});

module.exports = Mongoose.model('Isogram', isogramSchema);