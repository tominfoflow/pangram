const Mongoose = require('mongoose')
const Schema = Mongoose.Schema

const anagramSchema = new Schema({
    key : String,
    anagrams : [String]
});

module.exports = Mongoose.model('Anagram', anagramSchema);