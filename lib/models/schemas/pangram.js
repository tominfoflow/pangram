const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const pangramSchema = new Schema({
    // book: { type: String, trim: true, required: true, lowercase: true },
    text: { type: String, trim: true }
})

// const pangramModel = Mongoose.model('pangram', pangramSchema);

module.exports = Mongoose.model('pangram', pangramSchema);