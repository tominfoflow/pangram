const IsogramSchema = require('./schemas/isogram')

const IsogramModel = {};

IsogramModel.addIsogram = function(payload){
    IsogramSchema.collection.insertOne(payload, function(err, docs){
        if(err){
            console.log(err);
        } 
    })
}

IsogramModel.addIsograms = function(payload){
    IsogramSchema.collection.insertMany(payload, function(err, docs){
        if(err){
            console.log(err);
        } 
    })
}

IsogramModel.randomIsogram = async function(){
    return IsogramSchema.aggregate().sample(1);
}

module.exports = IsogramModel;