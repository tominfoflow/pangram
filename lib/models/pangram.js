const PangramModel = require('./schemas/pangram')

const internals = {};

internals.createPangram = function (request, h) {
    request.save().then(doc => { console.log(doc) }).catch(err => console.log(err));
}

internals.fetchPangram = async function (request, h) {
    return await PangramModel.find()
        .then((result) => { 
            const res = result[Math.floor(Math.random() * result.length)];
            return res.text;
        })
        .catch((err) => {
            console.log(err);
        });
}

internals.editPangram = async function(request, h){
    return
}

module.exports = internals;

