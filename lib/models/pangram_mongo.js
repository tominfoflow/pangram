const mongo = require('mongodb')
const mongoClient = require('mongodb').MongoClient;
const url = "mongodb+srv://admin:admin@cluster0-zbtkd.mongodb.net/test?retryWrites=true&w=majority"

const internals = {};

internals.insertPangram = async function (obj) {
    mongo.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }).then(async function (mongoClient) {
        await mongoClient.db('Cluster0').collection('pangrams').insert(obj);
    })
}

internals.getOnePangramMongo = async function () {
    return mongo.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }).then(async function (mongoClient) {
        const result = await mongoClient.db('Cluster0').collection('pangrams').findOne();
        return result;
    });
}

internals.getRandomPangramMongo = async function () {
    return mongo.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }).then(async function (mongoClient) {
        const result = await mongoClient.db('Cluster0').collection('pangrams').find().toArray();
        return result[Math.floor(Math.random() * result.length)];
    });
}

module.exports = internals;