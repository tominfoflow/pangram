const isogram = {};

isogram.isIsogram = function(word){
    wordWithoutSpaceAndHyphens = word.split('-').join('').split(' ').join('');

    const sizeSetWord = new Set(wordWithoutSpaceAndHyphens).size;
    const sizeWord = [...wordWithoutSpaceAndHyphens].length;

    return (sizeSetWord === sizeWord);
}

module.exports = isogram;